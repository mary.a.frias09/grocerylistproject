package com.grocery.demo.controller;
import com.grocery.demo.model.Grocery;
import com.grocery.demo.services.GroceryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
public class GroceryController {

    @Autowired
    GroceryService groceryService;

    @GetMapping("/groceries")
    public String viewHomePage(Model model){
        return findPaginated(1,"name", "asc", model);
    }


    @GetMapping("/showNewGroceryForm")
    public String showNewGroceryForm(Model model){
        Grocery grocery = new Grocery();
        model.addAttribute("grocery", grocery);
        return "/groceries/add_grocery";
    }


    @PostMapping("/saveGrocery")
    public String saveGrocery(@ModelAttribute("grocery") Grocery grocery) {
        groceryService.saveGrocery(grocery);
        return "redirect:/groceries";
    }


    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id")long id, Model model){
        Grocery grocery = groceryService.getGroceryById(id);

        model.addAttribute("grocery", grocery);
        return "update_grocery";
    }

    @GetMapping("/deleteGrocery/{id}")
    public String deleteGrocery(@PathVariable(value="id")long id){
        // call delete grocery method
        this.groceryService.deleteGroceryById(id);
        return "redirect:/";
    }


    @GetMapping("/page/{pageNo}")//@RequestParam is a Spring annotation used to bind a web request parameter to a method parameter. It has the following optional elements:
    public String findPaginated(@PathVariable(value="pageNo") int pageNo, @RequestParam("sortField")String sortField, @RequestParam("sortDir") String sortDir, Model model) {

        int pageSize = 5;

        Page<Grocery> page=groceryService.findPaginated(pageNo,pageSize,sortField,sortDir);

        List<Grocery> listGrocery=page.getContent();

        model.addAttribute("currentPage", pageNo);//mode
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("sort", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc")?"desc":"asc");
        model.addAttribute("listGrocery", listGrocery);
        return "index";
    }
}
//    In your Controller, create a GetMapping and assign “/groceries” as the url
//    This controller method returns is passing in a model as a parameter,
//    and we’re adding it as an Attribute (.addAttribute())
//        - Return this to “groceries/index”
//        Create a GetMapping and assign “groceries/create” as the url
//        - Create a method that passes Model model as the parameter and adds
//        model as an Attribute for a new Grocery being created
//        - Return “groceries/create”
//        Create a PostMapping and assign the “groceries/create” as the url
//        - Create a method that will take the newly created Grocery, and saves
//        it
//        - Redirect this to the index with all the groceries
